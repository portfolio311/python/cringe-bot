# Necessary python packages
try:
    import discord
    import os
    import platform
    from random import randint

except ModuleNotFoundError as e:
    from modules import installer

    # ANSI Stuff
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"

    print(f'{pref}0;{warning}' + 'Alert: ' + reset + 'a required package is not installed!')
    print('Do you want to install the missing packages? [y/n]')
    userIn = input(">> ")

    match userIn:
        case "y":
            # install missing packages
            installer.install()
        case "n":
            print('Ending program')
            quit(-1)

        case _:
            print(f'{pref}0;{error}' + 'Error: ' + reset + 'invalid input. Aborting')
            quit(-1)


# Custom Modules
try:
    from modules import reddit
    from modules import colors

except ModuleNotFoundError as e:
    pref = "\033["
    reset = f"{pref}0m"
    error = "31m"
    warning = "33m"
    print(e)
    print(f'{pref}0;{error}' + 'Error: ' + reset + 'module missing! Please download it from the following gitlab page: '
                                                   'https://gitlab.com/Mr_Kohli/cringe-bot')

    quit(-1)


# Discord Client
intents = discord.Intents.all()
intents.typing = True
intents.presences = True
client = discord.Client(intents=intents)


def clearConsole():
    if platform.system() == "Windows":
        os.system('cls')
    elif platform.system() == "Linux":
        os.system('clear')


def helper():
    subList = reddit.loadSubs()
    subs = ""

    for i in range(len(subList)):
        subs += subList[i] + "\n"

    msg = "This bot sends a video every time someone says *the word* a video.\n\n" \
          "It also offers you the following command:" \
          "```get-cringe```" \
          "Which sends a post from one of the following subs:" \
          "```" + subs + "```"

    return msg


def messageHandler(message):
    # cleans message
    messageSend = message.content.lower()
    messageSend = messageSend.split(' ')

    getCringe = False

    if any('get-cringe' in s for s in messageSend):
        return "getCringe"

    if any('get-help' in s for s in messageSend):
        return "help"

    if any('cringe' in s for s in messageSend):
        return "cringe"

    if any('cringé' in s for s in messageSend):
        return "cringe"

    if any('cr¡nge' in s for s in messageSend):
        return "cringe"


@client.event
async def on_ready():
    clearConsole()
    logo1 = "   _____ _____  _____ _   _  _____ ______   ____   ____ _______ \n"
    logo2 = "  / ____|  __ \|_   _| \ | |/ ____|  ____| |  _ \ / __ \__   __|\n"
    logo3 = " | |    | |__) | | | |  \| | |  __| |__    | |_) | |  | | | |   \n"
    logo4 = " | |    |  _  /  | | | . ` | | |_ |  __|   |  _ <| |  | | | |   \n"
    logo5 = " | |____| | \ \ _| |_| |\  | |__| | |____  | |_) | |__| | | |   \n"
    logo6 = "  \_____|_|  \_\_____|_| \_|\_____|______| |____/ \____/  |_|   \n"
    print(f'{colors.pref}0;{colors.logo}' + logo1 + logo2 + logo3 + logo4 + logo5 + logo6 + colors.reset)
    print('Cringe Bot is now' + f'{colors.pref}0;{colors.green}' + ' online' + colors.reset + ' as' +
          f'{colors.pref}0;{colors.user}' + ' {0.user}'.format(client) + colors.reset)


@client.event
async def on_message_edit(before, message):
    # send message to the message handler
    response = messageHandler(message)

    # finds right response
    match response:
        # get-cringe
        case "getCringe":
            # Send status message due to long delay
            msg = "Getting some cringe from Reddit"
            await message.channel.send(msg)

            # Get cringe post link
            returns = reddit.requestImg()

            if returns[1] != "err":
                msg = "Sending cringe from: " + returns[1]
                await message.channel.send(msg)

                # Send cringe
                msg = returns[0]
                await message.channel.send(msg)

                # Write Protocol
                print(f'{colors.pref}0;{colors.user}' + str(
                    message.author) + colors.reset + " wanted to see the following cringe: "
                      + f'{colors.pref}0;{colors.message}' + msg + colors.reset)

            elif returns[1] == "err":
                msg = "An error has occurred, please try again."
                print(f'{pref}0;{error}' + 'Error: ' + reset + "Reddit API Error")
                await message.channel.send(msg)

        # get-help
        case "help":
            # Generating help message
            msg = helper()
            await message.channel.send(msg)

            # Write Protocol
            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " needs some help!")

        # cringe person
        case "cringe":
            # Open Video file
            f = open("data/cringe.mp4", "rb")
            # Convert video to a message
            video = discord.File(f)

            await message.channel.send('Oh no cringé!')
            await message.channel.send(file=video)
            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " is very cringe!")


@client.event
async def on_message(message):
    if message.author == client.user:
        return

        # send message to the message handler
    response = messageHandler(message)

    # finds right response
    match response:
        # get-cringe
        case "getCringe":
            # Send status message due to long delay
            msg = "Getting some cringe from Reddit"
            await message.channel.send(msg)

            # Get cringe post link
            returns = reddit.requestImg()

            if returns[1] != "err":
                msg = "Sending cringe from: " + returns[1]
                await message.channel.send(msg)

                # Send cringe
                msg = returns[0]
                await message.channel.send(msg)

                # Write Protocol
                print(f'{colors.pref}0;{colors.user}' + str(
                    message.author) + colors.reset + " wanted to see the following cringe: "
                      + f'{colors.pref}0;{colors.message}' + msg + colors.reset)

            elif returns[1] == "err":
                msg = "An error has occurred, please try again."
                print(f'{pref}0;{error}' + 'Error: ' + reset + "Reddit API Error")
                await message.channel.send(msg)

        # get-help
        case "help":
            # Generating help message
            msg = helper()
            await message.channel.send(msg)

            # Write Protocol
            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " needs some help!")

        # cringe person
        case "cringe":
            # Open Video file
            f = open("data/cringe.mp4", "rb")
            # Convert video to a message
            video = discord.File(f)

            await message.channel.send('Oh no cringé!')
            await message.channel.send(file=video)
            print(f'{colors.pref}0;{colors.user}' + str(message.author) + colors.reset + " is very cringe!")


if __name__ == '__main__':
    clearConsole()
    try:
        # Reads Bot-Token from file
        token = open("data/TOKEN.txt", "r")
        # Starts bot
        client.run(str(token.read()))
    except FileNotFoundError as e:
        print(f'{colors.pref}0;{colors.error}' + 'Error: ' + colors.reset + 'TOKEN.txt not found.\n'
              + 'Please see the README for help')
        quit(-1)
    except Exception as e:
        print(f'{colors.pref}0;{colors.error}' + 'Error: ' + colors.reset + 'the following error has occurred:\n' +
              str(e) + '\n\nThe program will now be terminated')
        quit(-1)
