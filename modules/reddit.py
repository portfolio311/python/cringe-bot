import requests
import urllib.request

from random import randint
from modules import colors


def initReddit():
    CLIENT_ID = 'cfNwJ5ALCJNjLgasw2wdyQ'
    CLIENT_SECRET = 'iBPi80-a6dowV0-AULrO5dI3elQokg'
    auth = requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET)

    data = {'grant_type': 'password',
            'username': 'KohlisKuhlerBot',
            'password': 'mqkKySaFkRHRF6Hq'}

    headers = {'User-Agent': 'MyBot/0.0.1'}

    res = requests.post('https://www.reddit.com/api/v1/access_token', auth=auth, data=data, headers=headers)

    TOKEN = res.json()['access_token']

    headers = {**headers, **{'Authorization': f"bearer {TOKEN}"}}

    requests.get('https://oauth.reddit.com/api/v1/me', headers=headers)

    return headers


def loadSubs():
    # Read subreddit list
    url = 'https://gitlab.com/Mr_Kohli/cringe-bot-subreddits/-/raw/main/subs.csv'
    response = urllib.request.urlopen(url)
    subs = [l.decode('utf-8') for l in response.readlines()]

    # Remove the "\n"
    for i in range(len(subs)):
        subs[i] = subs[i].replace("\n", "")

    return subs


def requestImg():
    headers = initReddit()

    subList = loadSubs()
    rand = randint(0, (len(subList) - 1))
    sub = subList[rand]
    #print(sub)

    res = requests.get("https://oauth.reddit.com/" + sub + "/hot", headers=headers)
    postList = []
    randPost = None
    try:
        for post in res.json()['data']['children']:
            # print(post['data']['url'])
            postList.append(post['data']['url'])

        randPost = randint(0, (len(postList) - 1))
    except KeyError:
        print(f'{colors.pref}0;{colors.error}' + 'Error: ' + colors.reset + 'got an invalid answer from the server')
        sub = "err"

    return [postList[randPost], sub]
