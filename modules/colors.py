# ANSI Stuff
pref = "\033["
reset = f"{pref}0m"


# Color Codes
black = "30m"
error = "31m"
green = "32m"
message = "33m"
logo = "34m"
magenta = "35m"
user = "36m"
white = "37m"
notification = "34m"
